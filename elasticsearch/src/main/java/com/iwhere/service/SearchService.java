package com.iwhere.service;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iwhere.model.AdministrativeDivision;
import com.iwhere.model.Countrycode;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestResult;
import io.searchbox.core.Bulk;
import io.searchbox.core.Index; 
import io.searchbox.indices.DeleteIndex;
import io.searchbox.indices.IndicesExists;

/**
 * 将数据导入到elasticsearch中
 * @author niucaiyun
 *
 */
@Service
public class SearchService {

	@Autowired
	private JestClient jestClient;
	/**
	 * 将mysql数据库中数据，录入到es中
	 */
	public void builderSearchIndexdivision(){
		String indexName= "administrative_division_bf";
		Connection conn = null;
  	    PreparedStatement ps = null;
  	    ResultSet rs = null;
		int count=0;
		long start = System.currentTimeMillis();  
	        try {
	         
	            Bulk.Builder bulk = new Bulk.Builder();
	            //添加数据去服务端es
	            /*查询数据库中的数据start*/
	  			Class.forName("com.mysql.jdbc.Driver");
	  			String url = "jdbc:mysql://192.168.52.61:3306/administrative_division";
	  			/*String url = "jdbc:mysql://127.0.0.1:3306/poi";*/
	  	        conn = (Connection) DriverManager.getConnection(url, "root", "root");
	  	        System.out.println("写入数据开始，成功连接MySQL：division");
	  	        /* String sql = "select * from administrative_division_bf";*/
	  	        String sql = "select * from administrative_division_bf";
	  	        ps = (PreparedStatement) conn.prepareStatement(sql,ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
	  	        ps.setFetchSize(Integer.MIN_VALUE);
	  	        rs = ps.executeQuery();
	  	        /*查询数据库中的数据end*/
	  	         while(rs.next()){   	 
	  	        	 /*Division division = new Division();
	  	        	division.setID(Integer.valueOf(rs.getString(1)));
	  	        	division.setName(rs.getString(2));
	  	        	division.setIntroduce(rs.getString(3));
	  	        	division.setAdmin_code(rs.getString(4));
	  	        	division.setType(rs.getString(5));
	  	        	division.setType_chinese(rs.getString(6));
	  	        	division.setProcess_flg(rs.getString(7));*/
	  	        	
	  	        	AdministrativeDivision ad = new AdministrativeDivision();
	  	        	ad.setId(Integer.valueOf(rs.getString(1)));
	  	        	ad.setName(rs.getString(2)==null ? " " : rs.getString(2));
	  	        	ad.setMembership(rs.getString(3)==null ? " " : rs.getString(3));
	  	        	ad.setIntroduce(rs.getString(4)==null ? " " : rs.getString(4));
	  	        	ad.setAdministrativedivision(rs.getString(5)==null ? " " : rs.getString(5));
	  	        	ad.setType(rs.getString(6)==null ? 0 : Integer.valueOf(rs.getString(6)));
	  	        	ad.setTypechinese(rs.getString(7)==null ? " " : rs.getString(7));
	  	        	ad.setProcessflg(rs.getString(8)==null ? " " : rs.getString(8));
	  	        	ad.setPostcode(rs.getString(9)==null ? " " : rs.getString(9));
	  	        	ad.setCarnum(rs.getString(10)==null ? " " : rs.getString(10));
	  	        	ad.setTelnum(rs.getString(11)==null ? " " : rs.getString(11));
	  	        	ad.setPopulation(rs.getString(12)==null ? " " : rs.getString(12));
	  	        	ad.setPopulationdensity(rs.getString(13)==null ? " " : rs.getString(13));
	  	        	ad.setCompound(rs.getString(14)==null ? " " : rs.getString(14));
	  	        	ad.setLng(rs.getString(15)==null?" ":rs.getString(15));
	  	        	ad.setLat(rs.getString(16)==null?" ":rs.getString(16));
	  	        	count++;
	  	            Index index = new Index.Builder(ad).index(indexName).type(indexName).build();
	  	            bulk.addAction(index);
	  	            System.out.println(count);
	  	         }
	  	         jestClient.execute(bulk.build());
	  	         System.gc();
	  	        //}
	  	       
	  		} catch (Exception e) {
	  			// TODO Auto-generated catch block
	  			e.printStackTrace();
	  		}finally {
	  			 if(rs!=null){
	  	        	 try {
						rs.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	  	        }
	  	        if(ps!=null){
	  	        	try {
						ps.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	  	        }
	  	        if(conn!=null){
	  	        	try {
						conn.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	  	        }
			}
	  		  long end = System.currentTimeMillis();  
	          System.out.println("创建索引时间:数据量是  " + count + "记录,共用时间 -->> " + (end - start) + " 毫秒");  
	    }
	
	/**
	 * 将mysql数据库中数据，录入到es中
	 */
	public void builderSearchIndexcountrycode(){
		
		String indexName= "china";
		Connection conn = null;
  	    PreparedStatement ps = null;
  	    ResultSet rs = null;
		int count=0;
		int i;
		int a;
		long start = System.currentTimeMillis();  
	        try {
	            Bulk.Builder bulk = new Bulk.Builder();
	            //添加数据去服务端es
	            /*查询数据库中的数据start*/
	  			Class.forName("com.mysql.jdbc.Driver");
	  			String url = "jdbc:mysql://192.168.52.61:3306/countrycode";
	  	        conn = (Connection) DriverManager.getConnection(url, "root", "root");
	  	        
	  	        System.out.println("写入数据开始，成功连接MySQL：countrycode");
	  	        for(i=10;i<=30;i++){
	  	        	a=i*500000;
	  	        	System.out.println(i);
	  	        	String sql = "select id,geo_num,geo_level,country_code,country_name from countrycode_china limit "+a+",500000";
	  	        	ps = (PreparedStatement) conn.prepareStatement(sql,ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
	  	        	ps.setFetchSize(Integer.MIN_VALUE);
	  	        	// ps.setQueryTimeout(1000000);
	  	        	rs = ps.executeQuery();
	  	        	/*查询数据库中的数据end*/
	  	        	while(rs.next()){
	  	        		Countrycode countrycode = new Countrycode();
	  	        		countrycode.setId(Integer.valueOf(rs.getString(1)));
	  	        		countrycode.setGeo_num(rs.getString(2));
	  	        		countrycode.setGeo_level(Integer.valueOf(rs.getString(3)));
	  	        		countrycode.setCountry_code(rs.getString(4));
	  	        		countrycode.setCountry_name(rs.getString(5));
	  	        		count++;
	  	        		Index index = new Index.Builder(countrycode).index(indexName).type("countrycode").build();
	  	        		bulk.addAction(index);
	  	        		System.out.println(count);
	  	        	}
	  	         jestClient.execute(bulk.build());
	  	         System.gc();
	  	        }
	  	       
	  		} catch (Exception e) {
	  			// TODO Auto-generated catch block
	  			e.printStackTrace();
	  		}finally {
	  			 if(rs!=null){
	  	        	 try {
						rs.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	  	        }
	  	        if(ps!=null){
	  	        	try {
						ps.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	  	        }
	  	        if(conn!=null){
	  	        	try {
						conn.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	  	        }
			}
		long end = System.currentTimeMillis();
		System.out.println("创建索引时间:数据量是  " + count + "记录,共用时间 -->> " + (end - start) + " 毫秒");
	    }
	
	public void deleteSearchdivision() {
		// TODO Auto-generated method stub
		String indexName = "administrative_division_bf1";
		IndicesExists indicesExists = new IndicesExists.Builder(indexName).build();
		try {
			JestResult isIndexExist = jestClient.execute(indicesExists);
			DeleteIndex index = new DeleteIndex.Builder(indexName).build();
			JestResult result = jestClient.execute(index);
			System.out.println("===索引删除成功");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
         
	}
	
	public void deleteSearchcountry() {
		// TODO Auto-generated method stub
		String indexName = "countrycode_japan_gdsys";
		IndicesExists indicesExists = new IndicesExists.Builder(indexName).build();
		try {
			JestResult isIndexExist = jestClient.execute(indicesExists);
			DeleteIndex index = new DeleteIndex.Builder(indexName).build();
			JestResult result = jestClient.execute(index);
			System.out.println("===索引删除成功");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

