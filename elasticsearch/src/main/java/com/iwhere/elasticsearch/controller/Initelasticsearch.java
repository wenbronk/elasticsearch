package com.iwhere.elasticsearch.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.iwhere.service.SearchService;


@Controller
@RequestMapping("/")
public class Initelasticsearch {
	
	@Autowired
	SearchService searchService;
	
	/***
	 * es中录入数据division
	 * @return
	 *
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/search/division")
	public ModelAndView createInitialDatadivision() {
		searchService.builderSearchIndexdivision();
		//ModelAndView mv = new ModelAndView("forward:/");
		//mv.addObject("message", "索引已创建成功!");
		return null;
	}
	
	/***
	 * es中录入数据country
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/search/countrycode")
	public ModelAndView createInitialDatacountrycode() {
		//http://localhost:8080/elasticsearchtool/search/countrycode
		searchService.builderSearchIndexcountrycode();
		//ModelAndView mv = new ModelAndView("forward:/");
		//mv.addObject("message", "索引已创建成功!");
		return null;
	}
	
     /***
	 * 根据索引删除es中country数据
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/delete/deletecountry")
	public ModelAndView deleteSearchcountry() {
		searchService.deleteSearchcountry();
		return null;
	}
	
	/***
	 * 根据索引删除es中country数据
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/delete/division")
	public ModelAndView deleteindex() {
		searchService.deleteSearchdivision();
		return null;
	}
	

}
