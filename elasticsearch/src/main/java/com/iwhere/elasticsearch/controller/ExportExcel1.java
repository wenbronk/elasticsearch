package com.iwhere.elasticsearch.controller;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExportExcel1 {
	// HSSF是POI工程对Excel 97(-2007)文件操作的纯Java实现
	// XSSF是POI工程对Excel 2007 OOXML (.xlsx)文件操作的纯Java实现
	public static void main(String[] args) {
		Workbook wb = new XSSFWorkbook(); // 创建文档对象
		Sheet sheet = wb.createSheet("单元格样式测试"); // 表单
		// 创建行 第一行
		for (int i = 0; i <= 100; i++) {
			Row Row1 = sheet.createRow(i);
			// 创建单元格 第一列
			Cell A1_1Cell = Row1.createCell((short) 0);
			// 在单元格中写入内容
			A1_1Cell.setCellValue(i);
		}
		// **************字体颜色********************
		// 创建行 第4行
		Row Row4 = sheet.createRow((int) 3);
		// 创建单元格 第4列
		Cell A4_4Cell = Row4.createCell((short) 3);
		A4_4Cell.setCellValue("单元格字体");
		CellStyle fontStyle = wb.createCellStyle(); // 格子单元样式
		Font font = wb.createFont(); // 字体
		// 设置字体为斜体字
		font.setItalic(true);
		// 将字体设置为“红色”
		font.setColor(Font.COLOR_RED);
		// 将字体大小设置为22号
		font.setFontHeightInPoints((short) 22);
		// 将“华文行楷”字体应用到当前单元格上
		font.setFontName("华文行楷");
		// 添加（Font.U_SINGLE单条下划线/Font.U_DOUBLE双条下划线）
		font.setUnderline(Font.U_DOUBLE);
		// 是否添加删除线
		font.setStrikeout(true);
		// 将字体应用到样式上面
		fontStyle.setFont(font);
		// 将样式应用到单元格里
		A4_4Cell.setCellStyle(fontStyle);
		// =========================================
		// **************设置单元格边框********************
		// 创建行 第6行
		Row Row6 = sheet.createRow((int) 5);
		// 创建单元格 第2列
		Cell A6_2Cell = Row6.createCell((short) 1);
		A6_2Cell.setCellValue("单元格边框");
		CellStyle borderStyle = wb.createCellStyle();
		// CellStyle.BORDER_DOUBLE 双边线
		// CellStyle.BORDER_THIN 细边线
		// CellStyle.BORDER_MEDIUM 中等边线
		// CellStyle.BORDER_DASHED 虚线边线
		// CellStyle.BORDER_HAIR 小圆点虚线边线
		// CellStyle.BORDER_THICK 粗边线
		borderStyle.setBorderRight(XSSFCellStyle.BORDER_THICK);
		borderStyle.setBorderTop(XSSFCellStyle.BORDER_THICK);
		borderStyle.setBorderLeft(XSSFCellStyle.BORDER_THICK);
		borderStyle.setBorderBottom(XSSFCellStyle.BORDER_THICK);
		// 设置单元格边框颜色
		borderStyle.setTopBorderColor(IndexedColors.RED.getIndex());
		borderStyle.setLeftBorderColor(HSSFColor.BLUE.index);
		borderStyle.setRightBorderColor(IndexedColors.ORANGE.getIndex());
		borderStyle.setBottomBorderColor(IndexedColors.GOLD.getIndex());
		A6_2Cell.setCellStyle(borderStyle);
		// **********************单元格高度和宽度********************
		// 创建行 第10行
		Row Row10 = sheet.createRow((int) 9);
		// 创建单元格 第3列
		Cell A10_3Cell = Row10.createCell((short) 2);
		CellStyle sizeCellStyle = wb.createCellStyle();
		// 设置行的高度
		Row10.setHeightInPoints(50);
		// 单元格内容
		String sizeCellValue = "单元格高度和宽度测试";
		A10_3Cell.setCellValue(sizeCellValue);
		// 单元格宽度 3 代表第二列
		sheet.setColumnWidth(2, (sizeCellValue.getBytes().length) * 256);
		A10_3Cell.setCellStyle(sizeCellStyle);
		// **************单元格自动换行******************
		// 创建行 第13行
		Row Row13 = sheet.createRow((int) 12);
		// 创建单元格 第4列
		Cell A13_4Cell = Row13.createCell((short) 3);
		CellStyle cellContent = wb.createCellStyle();
		A13_4Cell.setCellValue("路漫漫其修远兮，吾将上下而求索。");
		cellContent.setWrapText(true); // 设置单元格内容是否自动换行
		A13_4Cell.setCellStyle(cellContent);
		// ************合并单元格行*************
		// 创建行 第15行
		Row Row15 = sheet.createRow((int) 14);
		// 创建单元格 第4列
		Cell A15_3Cell = Row15.createCell((short) 2);
		A15_3Cell.setCellValue("合并单元格行");
		// CellStyle rowStyle = wb.createCellStyle();
		// rowStyle.setBorderRight(XSSFCellStyle.BORDER_THICK);
		// rowStyle.setBorderTop(XSSFCellStyle.BORDER_THICK);
		// rowStyle.setBorderLeft(XSSFCellStyle.BORDER_THICK);
		// rowStyle.setBorderBottom(XSSFCellStyle.BORDER_THICK);
		// 合并15行ABC三列
		// 起始行号,终止行号, 起始列号,终止列号
		CellRangeAddress region = new CellRangeAddress(14, 14, 2, 5);
		sheet.addMergedRegion(region);
		// A15_3Cell.setCellStyle(rowStyle);
		// ************合并单元格列*************
		// 创建行 第15行
		Row Row17 = sheet.createRow((int) 16);
		// 创建单元格 第4列
		Cell A17_4Cell = Row17.createCell((short) 4);
		A17_4Cell.setCellValue("合并单元格列");
		// 合并15行ABC三列
		// 起始行号,终止行号, 起始列号,终止列号
		CellRangeAddress region1 = new CellRangeAddress(16, 18, 4, 4);
		sheet.addMergedRegion(region1);

		// ************合并单元格行和列*************
		// 创建行 第15行
		Row Row20 = sheet.createRow((int) 19);
		// 创建单元格 第4列
		Cell A20_6Cell = Row20.createCell((short) 6);
		A20_6Cell.setCellValue("合并单元格行和列");
		// 合并15行ABC三列
		// 起始行号,终止行号, 起始列号,终止列号
		CellRangeAddress region2 = new CellRangeAddress(19, 22, 6, 8);
		sheet.addMergedRegion(region2);

		// *************单元格样式*****************
		Row Row25 = sheet.createRow((int) 24);
		// 创建单元格 第4列
		Cell A25_4Cell = Row25.createCell((short) 3);
		A25_4Cell.setCellValue("单元格样式");
		CellStyle cellStyle = wb.createCellStyle();
		// 设置单元格内容水平对其方式
		// XSSFCellStyle.ALIGN_CENTER 居中对齐
		// XSSFCellStyle.ALIGN_LEFT 左对齐
		// XSSFCellStyle.ALIGN_RIGHT 右对齐
		cellStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);

		// 设置单元格内容垂直对其方式
		// XSSFCellStyle.VERTICAL_TOP 上对齐
		// XSSFCellStyle.VERTICAL_CENTER 中对齐
		// XSSFCellStyle.VERTICAL_BOTTOM 下对齐
		cellStyle.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		// 使用以上样式
		A25_4Cell.setCellStyle(cellStyle);
		// 起始行号,终止行号, 起始列号,终止列号
		CellRangeAddress region3 = new CellRangeAddress(24, 26, 3, 4);
		sheet.addMergedRegion(region3);
		// 写入excel文件
		try {
			FileOutputStream fileOut = new FileOutputStream("E:/Result1.xlsx");
			wb.write(fileOut);
			fileOut.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
