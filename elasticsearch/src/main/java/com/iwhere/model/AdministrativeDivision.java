package com.iwhere.model;

import io.searchbox.annotations.JestId;

public class AdministrativeDivision {
	@JestId
	private Integer id;
	private String name;
	private String membership;
	private String introduce;
	private String administrativedivision;
	private Integer type;
	private String typechinese;
	private String processflg;
	private String postcode;
	private String carnum;
	private String telnum;
	private String population;
	private String populationdensity;
	private String compound;
	private String lng;
	private String lat;
	public AdministrativeDivision() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMembership() {
		return membership;
	}
	public void setMembership(String membership) {
		this.membership = membership;
	}
	public String getIntroduce() {
		return introduce;
	}
	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}
	public String getAdministrativedivision() {
		return administrativedivision;
	}
	public void setAdministrativedivision(String administrativedivision) {
		this.administrativedivision = administrativedivision;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getTypechinese() {
		return typechinese;
	}
	public void setTypechinese(String typechinese) {
		this.typechinese = typechinese;
	}
	public String getProcessflg() {
		return processflg;
	}
	public void setProcessflg(String processflg) {
		this.processflg = processflg;
	}
	public String getPostcode() {
		return postcode;
	}
	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}
	public String getCarnum() {
		return carnum;
	}
	public void setCarnum(String carnum) {
		this.carnum = carnum;
	}
	public String getTelnum() {
		return telnum;
	}
	public void setTelnum(String telnum) {
		this.telnum = telnum;
	}
	public String getPopulation() {
		return population;
	}
	public void setPopulation(String population) {
		this.population = population;
	}
	public String getPopulationdensity() {
		return populationdensity;
	}
	public void setPopulationdensity(String populationdensity) {
		this.populationdensity = populationdensity;
	}
	public String getCompound() {
		return compound;
	}
	public void setCompound(String compound) {
		this.compound = compound;
	}
	public String getLng() {
		return lng;
	}
	public void setLng(String lng) {
		this.lng = lng;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}

}
