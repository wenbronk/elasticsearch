package com.iwhere.model;

import io.searchbox.annotations.JestId;

public class Countrycode {
	@JestId
	private int id;
	private String geo_num;
	private int geo_level;
	private String country_code;
	private String country_name;
	private String description_code;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getGeo_num() {
		return geo_num;
	}

	public void setGeo_num(String geo_num) {
		this.geo_num = geo_num;
	}

	public int getGeo_level() {
		return geo_level;
	}

	public void setGeo_level(int geo_level) {
		this.geo_level = geo_level;
	}

	public String getCountry_code() {
		return country_code;
	}

	public void setCountry_code(String country_code) {
		this.country_code = country_code;
	}

	public String getCountry_name() {
		return country_name;
	}

	public void setCountry_name(String country_name) {
		this.country_name = country_name;
	}

	public String getDescription_code() {
		return description_code;
	}

	public void setDescription_code(String description_code) {
		this.description_code = description_code;
	}

}
