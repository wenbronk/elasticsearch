package com.iwhere.model;

import io.searchbox.annotations.JestId;
public class Division {
	
    @JestId
	private int ID;
	private String name;
	private String introduce;
	private String admin_code;
	private String type;
	private String type_chinese;
	private String process_flg;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getType_chinese() {
		return type_chinese;
	}
	public void setType_chinese(String type_chinese) {
		this.type_chinese = type_chinese;
	}
	public String getProcess_flg() {
		return process_flg;
	}
	public void setProcess_flg(String process_flg) {
		this.process_flg = process_flg;
	}
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIntroduce() {
		return introduce;
	}
	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}
	public String getAdmin_code() {
		return admin_code;
	}
	public void setAdmin_code(String admin_code) {
		this.admin_code = admin_code;
	}
	
}
