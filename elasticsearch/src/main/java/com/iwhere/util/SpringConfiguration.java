package com.iwhere.util;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.config.HttpClientConfig;
/***
 * mysql数据录入到es中，配置文件
 * @author niucaiyun
 *
 */
@Configuration
public class SpringConfiguration {
	public @Bean HttpClientConfig httpClientConfig() {
		// String connectionUrl = "http://localhost:9200";
		String connectionUrl = "http://192.168.51.122:9200";
		//String connectionUrl = "http://192.168.51.236:9200";
		//String connectionUrl = "http://192.168.50.202:9200";
		//String connectionUrl = "http://101.200.174.96:9200";
		//String connectionUrl = "http://101.200.174.96:9200";
		//String connectionUrl = "http://192.168.50.169:9200";
		HttpClientConfig httpClientConfig = new HttpClientConfig.Builder(connectionUrl).multiThreaded(true).build();
		return httpClientConfig;
	}

	public @Bean JestClient jestClient() {
		JestClientFactory factory = new JestClientFactory();
		factory.setHttpClientConfig(httpClientConfig());
		return factory.getObject();
	}
}
