package com.iwhere.springES;

import java.util.ArrayList;
import java.util.List;

import javax.management.RuntimeErrorException;

import org.elasticsearch.action.ActionFuture;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthRequest;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.cluster.health.ClusterHealthStatus;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.CriteriaQuery;
import org.springframework.data.elasticsearch.core.query.DeleteQuery;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.data.elasticsearch.core.query.IndexQueryBuilder;

import com.iwhere.springES.util.APP;
import com.iwhere.springES.vo.NewsInfo;
import com.iwhere.springES.vo.TaskInfo;

/**
 * 测试spring整合  elasticSearchTemplate
 * @author 231
 */
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(value="classpath:spring/applicationContext-escluster.xml")
public class SpringEsTestCUD {

//	@Autowired
	private ElasticsearchTemplate elasticsearchTemplate;
//	@Autowired
	private Client client;
	@Before
	public void setelasticsearchtemplate() {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring/applicationContxt-escluster.xml");
		elasticsearchTemplate = context.getBean(ElasticsearchTemplate.class);
		client = elasticsearchTemplate.getClient();
	}
	
	/**
	 * 创建index, 创建mapping
	 * 判断index是否存在
	 * indexExists("typeName"), indexExists(Class<T> clazz);
	 */
	@Test
	public void init() {
		if (!elasticsearchTemplate.indexExists("indexName")) {
//			elasticsearchTemplate.indexExists(SpringEsTest.class);
			elasticsearchTemplate.createIndex(APP.ESProp.INDEX_NAME);
		}
		elasticsearchTemplate.putMapping(TaskInfo.class);
		elasticsearchTemplate.putMapping(NewsInfo.class);
	}
	
	public boolean insertOrUpdateTaskInfo(TaskInfo taskInfo) {
		IndexQuery indexQuery = new IndexQueryBuilder().withId(taskInfo.getTaskId()).withObject(taskInfo).build();
		elasticsearchTemplate.index(indexQuery);
		return true;
	}
	
	/**
	 * 批量插入或更新操作
	 */
	public boolean update(List<TaskInfo> taskInfoList) {
		ArrayList<IndexQuery> queryList = new ArrayList<IndexQuery>();
		for (TaskInfo taskInfo : taskInfoList) {
			IndexQuery indexQuery = new IndexQueryBuilder().withId(taskInfo.getTaskId())
				.withObject(taskInfo).build();
			queryList.add(indexQuery);
		}
		elasticsearchTemplate.bulkIndex(queryList);
		return true;
	}
	
	/**
	 * 批量插入或更新
	 * @param taskInfoList
	 * @return
	 */
	public boolean insertOrUpdateTaskInfo(List<TaskInfo> taskInfoList) {
		ArrayList<IndexQuery> queryList = new ArrayList<IndexQuery>();
		for (TaskInfo taskInfo : taskInfoList) {
			IndexQuery indexQuery = new IndexQueryBuilder().withId(taskInfo.getTaskId())
								.withObject(taskInfo).build();
			queryList.add(indexQuery);
		}
		elasticsearchTemplate.bulkIndex(queryList);
		return true;
	}
	
	/**
	 * 删除操作, 根据id删除
	 * 根据criteriaQuery删除
	 * 根据deleteCriteria删除
	 * @param id
	 * @param clazz
	 * @return
	 */
	public <T> boolean deleteById(String id, Class<T> clazz) {
		try {
			elasticsearchTemplate.delete(clazz, id);	// id
//			elasticsearchTemplate.delete(criteriaQuery, clazz);   //CriteriaQuery
//			elasticsearchTemplate.delete(deleteQuery);		// deleteQuery
		}catch (Exception e) {
			return false;
		}
		return true;
	}
	
	/**
	 * 检擦健康状态
	 * @return
	 */
	public boolean pingHealth() {
		ActionFuture<ClusterHealthResponse> health = client.admin().cluster().health(new ClusterHealthRequest());
		ClusterHealthStatus status = health.actionGet().getStatus();
		if (status.value() == ClusterHealthStatus.RED.value()) {
			throw new RuntimeException("elasticsearch Cluster health status is red");
		}
		return true;
	}
	
	@Test
	public void testGet() {
		System.out.println("123");
		GetResponse response = client.prepareGet("twitter", "tweet", "1")
					.get();
		
		System.out.println(response.getSourceAsString());
	}
}
