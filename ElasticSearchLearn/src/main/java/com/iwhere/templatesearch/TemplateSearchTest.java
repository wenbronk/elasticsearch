package com.iwhere.templatesearch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.InetSocketAddress;
import java.util.HashMap;

import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.node.Node;
import org.elasticsearch.node.NodeBuilder;
import org.elasticsearch.search.SearchHit;
import org.junit.Before;
import org.junit.Test;

/**
 * 使用模板查询
 * 
 * 用来建立索引挺不错的
 */
public class TemplateSearchTest {

	private TransportClient client;
	
	@Test
	public void createMapping() {
		
		
	}
	
	@Test
	public void testTemplate() {
		try {
			// 创建节点, 以节点的方式和集群进行交互
			Node node = NodeBuilder.nodeBuilder().clusterName("wenbronk_escluster")
					.client(true).node();	// 不存储数据
			Client client = node.client();
			
			// 读取模板, 设置参数
			BufferedReader reader = new BufferedReader(new FileReader(new File("/search.template")));
			StringBuilder sb = new StringBuilder();
			String line = null;
			while((line = reader.readLine()) != null) {
				sb.append(line);
				sb.append("\n");
			}
			
			HashMap<String, Object> hashMap = new HashMap<>();
			hashMap.put("from", 1);
			hashMap.put("size", 5);
			hashMap.put("key_words", "opencv sift");
			
			QueryBuilder queryBuilder = QueryBuilders.templateQuery(sb.toString(), hashMap);
			SearchRequestBuilder searchRequestBuilder = client.prepareSearch("blog_v1").setTypes("blogpost").setQuery(queryBuilder);
			SearchResponse searchResponse = searchRequestBuilder.get();
			
			for(SearchHit hit : searchResponse.getHits()) {
				System.out.println(hit.getSourceAsString());
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Before
	public void testBefore() {
		Settings settings = Settings.settingsBuilder().put("cluster.name", "wenbronk_escluster")
					.put("client.transport.sniff", true).build();
		client = TransportClient.builder().settings(settings).build()
				 .addTransportAddress(new InetSocketTransportAddress(new InetSocketAddress("192.168.50.37", 9300)));
		System.out.println("success to connect escluster");
	}
}
