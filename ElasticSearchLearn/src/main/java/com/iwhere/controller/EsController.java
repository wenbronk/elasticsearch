package com.iwhere.controller;

import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class EsController {

	private static Logger LOGGER = LoggerFactory.getLogger(EsController.class);
	
	@Autowired
	private Client client;
	
	@RequestMapping("/test")
	public @ResponseBody String test() {
		
		IndexResponse response = client.prepareIndex("twitter", "tweet").execute().actionGet();
		
		System.out.println("connect success");
		LOGGER.debug("success");
		LOGGER.error("success");
		return "ok";
	}
	
}
