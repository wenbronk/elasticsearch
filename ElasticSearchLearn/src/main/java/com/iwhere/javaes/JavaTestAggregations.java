package com.iwhere.javaes;

import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.geo.GeoPoint;
import org.elasticsearch.common.unit.DistanceUnit;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.script.Script;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.global.GlobalBuilder;
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogramInterval;
import org.elasticsearch.search.aggregations.bucket.significant.SignificantTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsBuilder;
import org.elasticsearch.search.aggregations.metrics.MetricsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.avg.Avg;
import org.elasticsearch.search.aggregations.metrics.geobounds.GeoBounds;
import org.elasticsearch.search.aggregations.metrics.geobounds.GeoBoundsBuilder;
import org.elasticsearch.search.aggregations.metrics.max.Max;
import org.elasticsearch.search.aggregations.metrics.min.Min;
import org.elasticsearch.search.aggregations.metrics.percentiles.Percentile;
import org.elasticsearch.search.aggregations.metrics.percentiles.Percentiles;
import org.elasticsearch.search.aggregations.metrics.scripted.ScriptedMetric;
import org.elasticsearch.search.aggregations.metrics.stats.Stats;
import org.elasticsearch.search.aggregations.metrics.stats.extended.ExtendedStats;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import org.elasticsearch.search.aggregations.metrics.tophits.TopHits;
import org.elasticsearch.search.aggregations.metrics.tophits.TopHitsBuilder;
import org.elasticsearch.search.aggregations.metrics.valuecount.ValueCount;
import org.elasticsearch.search.sort.SortOrder;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;



/**
 * es中的聚合相当于sql中的group by
 * es聚合, 
 * 桶聚合: term聚合, range聚合, date聚合, IPV4聚合
 * @author 231
 */
public class JavaTestAggregations {
	
	private TransportClient client;

	/**
	 * prepareCount(index)
	 * 
	 */
	@Test
	public void testTermsAggre() {
		SearchRequestBuilder requestBuilder = client.prepareSearch("twtitter").setTypes("tweet")
								.setFrom(0).setSize(100);
		// 根据id进行聚合
		AggregationBuilder<TermsBuilder> termsBuilder = AggregationBuilders.terms("agg").field("id")	// 对哪个字段聚合
				.subAggregation(AggregationBuilders.topHits("top").setFrom(0).setSize(10))
				.size(100);
		
		// 设置其他条件
		SearchResponse response = requestBuilder.setQuery(
				QueryBuilders.boolQuery().must(QueryBuilders.matchPhraseQuery("name", "中学历史")))	//查询名字中带中学历史
				.addSort("id", SortOrder.DESC)  // 根据id排序
				.addAggregation(termsBuilder) 	// 添加聚合条件
				.setExplain(true).execute().actionGet();
		
		// 对response进行分析
		SearchHits hits = response.getHits();
		Terms agg = response.getAggregations().get("agg");
		
		System.out.println(agg.getBuckets().size());
		
		for (Terms.Bucket entry : agg.getBuckets()) {
			String key = (String) entry.getKey();
			long docCount = entry.getDocCount();
			System.out.println("key: " + key + "doc_count" + docCount);
			
			TopHits topHits = entry.getAggregations().get("top");
			for (SearchHit hit : topHits.getHits().getHits()) {
				System.err.println("id: " + hit.getId() + "_source: " + hit.getSource().get("id"));
			}
		}
		
		System.out.println(hits.getTotalHits());
		for(int i = 0; i < hits.getHits().length; i++) {
			System.out.println(hits.getHits()[i].getSource().get("product_name"));
			System.out.println(hits.getHits()[i].getSource().get("product_id"));
			System.out.println(hits.getHits()[i].getSource().get("category_name"));
			
		}
		
	}
	
	/**
	 * 使用range聚合
	 */
	@Test
	public void testRangeAggre() {
		AggregationBuilder rangeBuilder = AggregationBuilders.range("agg")
						.field("price").addUnboundedTo(50)
						.addRange(51, 100)		// 分成不同的桶
						.addRange(101, 1000)
						.addUnboundedFrom(1001);
	}
	
	/**
	 * avg, min, max, sum, 等用法
	 * @param response
	 */
	public void testAvg(SearchResponse response) {
		
		
		// avg
		AggregationBuilders.avg("avg_children").field("children");
		//结果处理
		Avg avg = response.getAggregations().get("avg_children");
		double value = avg.getValue();
		
		// min
		AggregationBuilders.min("min_children").field("children");
		Min min = response.getAggregations().get("min_children");
		min.getValue();
		
		// max
		AggregationBuilders.max("max_children").field("children");
		Max max = response.getAggregations().get("max_children");
		
		// sum
		AggregationBuilders.sum("sum_agg").field("height");
		Sum sum = response.getAggregations().get("sum_agg");
		
		// count
		AggregationBuilders.count("count_agg").field("height");
		ValueCount count = response.getAggregations().get("count_agg");
		
		// 添加子 聚合
		AggregationBuilders.terms("by_country").field("country")
			.subAggregation(AggregationBuilders.min("min_score").field("score"));
	}
	
	/**
	 * 统计
	 * stats, 一次课获取最大, 最小, 平均, 综合, 计数等
	 */
	@Test
	public void testStatsAggr() {
		SearchResponse response = client.prepareSearch("twitter")
				.addAggregation(AggregationBuilders.stats("agg").field("heith"))
				.execute().actionGet();
		
		// 结果分析
		Stats aggregation = response.getAggregations().get("agg");
		aggregation.getMin();
		aggregation.getMax();
		aggregation.getAvg();
		aggregation.getCount();
		aggregation.getSum();
	}
	
	/**
	 * extendsStats
	 * 添加sum_of_square, variance, std_deviation, std_deviation_bounds等
	 */
	@Test
	public void testExtendedStats() {
		SearchResponse response = client.prepareSearch("twitter")
				.addAggregation(AggregationBuilders.extendedStats("agg").field("heith"))
				.execute().actionGet();
		
		// 结果分析
		ExtendedStats aggr = response.getAggregations().get("agg");
		aggr.getMin();
		aggr.getMax();
		aggr.getAvg();
		aggr.getCount();
		aggr.getSum();
		aggr.getStdDeviation();
		aggr.getSumOfSquares();
		aggr.getVariance();
	}
	
	/**
	 * 百分比
	 * 还有Ranks Aggregation
	 * 用法相同, 不举例
	 */
	@Test
	public void testPercentile() {
		MetricsAggregationBuilder aggregation = AggregationBuilders
					                .percentiles("agg").field("height");
		MetricsAggregationBuilder percentiles = AggregationBuilders
									.percentiles("agg").field("height")
									.percentiles(1.0, 5.0, 10.0, 20.0, 30.0, 75.0, 90.0);
		// response 解析
		SearchResponse response = client.prepareSearch("twitter").addAggregation(percentiles).execute().actionGet();
		Percentiles aggr = response.getAggregations().get("agg");
		for (Percentile percentile : aggr) {
			double percent = percentile.getPercent();
			double value = percentile.getValue();
		}
	}
	
	/**
	 * Cardinality
	 */
	@Test
	public void testCardinalityAgg() {
		MetricsAggregationBuilder aggregation = AggregationBuilders
							                .cardinality("agg")
							                .field("tags");
		// Cardinality agg = response.getAggregation().get("agg");
	}
	
	/**
	 * geoBounds 地理信息坐标
	 */
	@Test
	public void testGeoBounds() {
		GeoBoundsBuilder wrapLongitude = AggregationBuilders.geoBounds("agg").field("address.location")
					.wrapLongitude(true);
		SearchResponse response = client.prepareSearch("test").addAggregation(wrapLongitude).execute().actionGet();
		GeoBounds agg = response.getAggregations().get("agg");
		GeoPoint bottomRight = agg.bottomRight();	// 右下
		GeoPoint topLeft = agg.topLeft();			//左上
		double lat = topLeft.getLat();
		double lon = topLeft.getLon();
	}
	
	
	
	/**
	 * 先聚合, 后过滤, 页面的条件查询
	 * postFilter 仅过滤搜索结果, 不影响聚合结果
	 */
	@Test
	public void testPostFilter() {
		SearchRequestBuilder requestBuilder = client.prepareSearch("twitter");
		requestBuilder.setQuery(
				QueryBuilders.boolQuery()
					.must(QueryBuilders.matchPhraseQuery("name", "历史")))
				.addSort("category_id", SortOrder.ASC)
				.addAggregation(AggregationBuilders.terms("field"))		// 根据字段进行聚合
				.setPostFilter(QueryBuilders.rangeQuery("price").gt(100).lt(5000))
				.setExplain(true).execute().actionGet();
		
	}
	
	/**
	 * top hits
	 */
	@Test
	public void testTopHits() {
		// 设置topHits
		TopHitsBuilder topHitsBuilder = AggregationBuilders.topHits("top")
							.setExplain(true)
							.setSize(1).setFrom(10);
		// 添加其他条件
		TermsBuilder termsBuilder = AggregationBuilders.terms("agg").field("user")
					.subAggregation(topHitsBuilder);
		SearchResponse response = client.prepareSearch("twitter")
					.setQuery(QueryBuilders.matchAllQuery())
					.addAggregation(termsBuilder).execute().actionGet();
		Terms terms = response.getAggregations().get("agg");
		for (Terms.Bucket entry : terms.getBuckets()) {
			String key = (String) entry.getKey();
			long docCount = entry.getDocCount();
			TopHits topHits = entry.getAggregations().get("top");
			
			System.err.println("key: " + key + ": " + docCount);
			
			for (SearchHit hit : topHits.getHits().getHits()) {
				System.out.println(hit.getSourceAsString());
			}
		}
	}

	/**
	 * Metric Aggre
	 * 需要引入Groovy
	 * 
	 * <dependency>
		    <groupId>org.codehaus.groovy</groupId>
		    <artifactId>groovy-all</artifactId>
		    <version>2.3.2</version>
		    <classifier>indy</classifier>
		</dependency>
	 * 
	 * 
	 */
	@Test
	public void testMetric() {
		
		// create aggregation request
		MetricsAggregationBuilder aggregation1 =
		        AggregationBuilders
		                .scriptedMetric("agg")
		                .initScript(new Script("_agg['heights'] = []"))
		                .mapScript(new Script("if (doc['gender'].value == \"male\") " +
		                        "{ _agg.heights.add(doc['height'].value) } " +
		                        "else " +
		                        "{ _agg.heights.add(-1 * doc['height'].value) }"));

		// create a combin script
		MetricsAggregationBuilder aggregation2 =
		        AggregationBuilders
		                .scriptedMetric("agg")
		                .initScript(new Script("_agg['heights'] = []"))
		                .mapScript(new Script("if (doc['gender'].value == \"male\") " +
		                        "{ _agg.heights.add(doc['height'].value) } " +
		                        "else " +
		                        "{ _agg.heights.add(-1 * doc['height'].value) }"))
		                .combineScript(new Script("heights_sum = 0; for (t in _agg.heights) { heights_sum += t }; return heights_sum"));
		
		// create a reduce script
		MetricsAggregationBuilder aggregation3 =
		        AggregationBuilders
		                .scriptedMetric("agg")
		                .initScript(new Script("_agg['heights'] = []"))
		                .mapScript(new Script("if (doc['gender'].value == \"male\") " +
		                        "{ _agg.heights.add(doc['height'].value) } " +
		                        "else " +
		                        "{ _agg.heights.add(-1 * doc['height'].value) }"))
		                .combineScript(new Script("heights_sum = 0; for (t in _agg.heights) { heights_sum += t }; return heights_sum"))
		                .reduceScript(new Script("heights_sum = 0; for (a in _aggs) { heights_sum += a }; return heights_sum"));
		
		// 处理
		SearchResponse response = client.prepareSearch().setQuery(QueryBuilders.matchAllQuery())
			.addAggregation(aggregation1).execute().actionGet();
		
		ScriptedMetric agg = response.getAggregations().get("agg");
		Object aggregation = agg.aggregation();	
		System.out.println(aggregation);
	}
	
	/**
	 * global Aggregation 全局聚合器
	 * 只能作为顶级聚合器使用, 
	 */
	@Test
	public void testGlobal() {
		GlobalBuilder subAggregation = AggregationBuilders.global("agg")
			.subAggregation(AggregationBuilders.terms("genders").field("genders"));
		// Global agg = reponse.getAggregation().get("agg");
		// agg.getDocCount();
	}
	
	/**
	 * Filter aggregation
	 */
	@Test
	public void testFilter() {
		AggregationBuilders.filter("agg")
			.filter(QueryBuilders.termQuery("gender", "male"));
//		Filter agg = response.getAggregation().get("agg");
//		agg.getDocCount();
	}
	
	/**
	 * filters aggregation
	 */
	@Test
	public void testFilters() {
		AggregationBuilders.filters("agg")
			.filter("men", QueryBuilders.termQuery("gender", "male"))
			.filter("women", QueryBuilders.termQuery("gender", "female"));
//		Filters agg = response.getAggregations().get("agg");
//		for (Filters.Bucket entry : agg.getBuckets()) {
//		    String key = entry.getKeyAsString();            // bucket key
//		    long docCount = entry.getDocCount();            // Doc count
//		    logger.info("key [{}], doc_count [{}]", key, docCount);
//		}
	}
	
	/**
	 * missing aggregation
	 * 因为缺值或值为null, 不能被别的匹配到的
	 * 需要结合其他aggregation使用
	 */
	@Test
	public void testMissing() {
		AggregationBuilders.missing("agg").field("gender");
//		Missing agg = response.getAggregations().get("agg");
//		agg.getDocCount(); // Doc count
	}
	
	/**
	 * nested aggregation
	 * 
	 */
	@Test
	public void testNestedAgg() {
		AggregationBuilders.nested("agg").path("resellers");
//		Nested agg = sr.getAggregations().get("agg");
//		agg.getDocCount(); // Doc count
	}
	
	@Test
	public void testNestedReverseAgg() {
		AggregationBuilder aggregation = AggregationBuilders.nested("agg").path("resellers")
			        .subAggregation(AggregationBuilders.terms("name").field("resellers.name")
			                        .subAggregation(
			                        		AggregationBuilders.reverseNested("reseller_to_product")
			                        )
			        );
//		Nested agg = reponse.getAggregations().get("agg");
//		Terms name = agg.getAggregations().get("name");
//		for (Terms.Bucket bucket : name.getBuckets()) {
//		    ReverseNested resellerToProduct = bucket.getAggregations().get("reseller_to_product");
//		    resellerToProduct.getDocCount(); // Doc count
//		}
	}
	
	/**
	 * 父桶到子桶的在聚合
	 */
	@Test
	public void testChildrenAggr() {
		AggregationBuilders.children("agg").childType("reseller");
//		Children agg = sr.getAggregations().get("agg");
//		agg.getDocCount(); // Doc count
	}
	
	/**
	 * 
	 */
	@Test
	public void testTerms() {
		AggregationBuilders.terms("genders").field("gender");
//		Terms genders = sr.getAggregations().get("genders");
//		for (Terms.Bucket entry : genders.getBuckets()) {
//		    entry.getKey();      // Term
//		    entry.getDocCount(); // Doc count
//		}
	}
	
	@Test
	public void testOrderAgg() {
		AggregationBuilders
		    .terms("genders")
		    .field("gender")
		    .order(Terms.Order.aggregation("avg_height", false))
		    .subAggregation(
		        AggregationBuilders.avg("avg_height").field("height")
		    );
		
	}
	
	/**
	 * 有意义的聚合
	 */
	public void testSinificantTermAgg() {
		AggregationBuilder aggregation =
		        AggregationBuilders
		                .significantTerms("significant_countries")
		                .field("address.country");

		// Let say you search for men only
		SearchResponse sr = client.prepareSearch()
		        .setQuery(QueryBuilders.termQuery("gender", "male"))
		        .addAggregation(aggregation)
		        .get();
		
		// sr is here your SearchResponse object
		SignificantTerms agg = sr.getAggregations().get("significant_countries");
		for (SignificantTerms.Bucket entry : agg.getBuckets()) {
		    entry.getKey();      // Term
		    entry.getDocCount(); // Doc count
		}
	}
	
	@Test
	public void testRangeAggr() {
		AggregationBuilder aggregation =
		        AggregationBuilders
		                .range("agg")
		                .field("height")
		                .addUnboundedTo(1.0f)               // from -infinity to 1.0 (excluded)
		                .addRange(1.0f, 1.5f)               // from 1.0 to 1.5 (excluded)
		                .addUnboundedFrom(1.5f);   
//		Range agg = sr.getAggregations().get("agg");
//		for (Range.Bucket entry : agg.getBuckets()) {
//		    String key = entry.getKeyAsString();             // Range as key
//		    Number from = (Number) entry.getFrom();          // Bucket from
//		    Number to = (Number) entry.getTo();              // Bucket to
//		    long docCount = entry.getDocCount();    // Doc count
//
//		    logger.info("key [{}], from [{}], to [{}], doc_count [{}]", key, from, to, docCount);
//		}
	}
	
	/**
	 * 
	 */
	@Test
	public void testDateRange() {
		AggregationBuilder aggregation =
		        AggregationBuilders
		                .dateRange("agg")
		                .field("dateOfBirth")
		                .format("yyyy")
		                .addUnboundedTo("1950")    // from -infinity to 1950 (excluded)
		                .addRange("1950", "1960")  // from 1950 to 1960 (excluded)
		                .addUnboundedFrom("1960"); // from 1960 to +infinity
		
		// 也可以这样
		AggregationBuilder dateRangeBuilder = AggregationBuilders.dateRange("agg")
						.field("date").format("yyyy")
						.addUnboundedTo("1970")
						.addRange("1970", "2000")
						.addRange("2000", "2010")
						.addUnboundedFrom("2009");
		
		// sr is here your SearchResponse object
//		Range agg = sr.getAggregations().get("agg");
//		for (Range.Bucket entry : agg.getBuckets()) {
//		    String key = entry.getKeyAsString();                // Date range as key
//		    DateTime fromAsDate = (DateTime) entry.getFrom();   // Date bucket from as a Date
//		    DateTime toAsDate = (DateTime) entry.getTo();       // Date bucket to as a Date
//		    long docCount = entry.getDocCount();                // Doc count
//
//		    logger.info("key [{}], from [{}], to [{}], doc_count [{}]", key, fromAsDate, toAsDate, docCount);
//		}
	}
	
	@Test
	public void testIpRange() {
		AggregationBuilder aggregation =
		        AggregationBuilders
		                .ipRange("agg")
		                .field("ip")
		                .addUnboundedTo("192.168.1.0")             // from -infinity to 192.168.1.0 (excluded)
		                .addRange("192.168.1.0", "192.168.2.0")    // from 192.168.1.0 to 192.168.2.0 (excluded)
		                .addUnboundedFrom("192.168.2.0");          // from 192.168.2.0 to +infinity
		/**
		 * AggregationBuilder aggregation =
        		AggregationBuilders
                .ipRange("agg")
                .field("ip")
                .addMaskRange("192.168.0.0/32")
                .addMaskRange("192.168.0.0/24")
                .addMaskRange("192.168.0.0/16");
		 */
		// sr is here your SearchResponse object
//		Range agg = sr.getAggregations().get("agg");
//		for (Range.Bucket entry : agg.getBuckets()) {
//		    String key = entry.getKeyAsString();            // Ip range as key
//		    String fromAsString = entry.getFromAsString();  // Ip bucket from as a String
//		    String toAsString = entry.getToAsString();      // Ip bucket to as a String
//		    long docCount = entry.getDocCount();            // Doc count
//
//		    logger.info("key [{}], from [{}], to [{}], doc_count [{}]", key, fromAsString, toAsString, docCount);
//		}
	}
	
	@Test
	public void testHistogramAgg() {
		AggregationBuilders.histogram("agg").field("height").interval(1);
//		Histogram agg = sr.getAggregations().get("agg");
//		for (Histogram.Bucket entry : agg.getBuckets()) {
//		    Long key = (Long) entry.getKey();       // Key
//		    long docCount = entry.getDocCount();    // Doc count
//
//		    logger.info("key [{}], doc_count [{}]", key, docCount);
//		}
	}
	
	@Test
	public void DateHistogramAgg() {
		AggregationBuilders.dateHistogram("agg").field("dateOfBirth")
				.interval(DateHistogramInterval.YEAR);	// 间隔
		
//		Histogram agg = sr.getAggregations().get("agg");
//		for (Histogram.Bucket entry : agg.getBuckets()) {
//		    DateTime key = (DateTime) entry.getKey();    // Key
//		    String keyAsString = entry.getKeyAsString(); // Key as String
//		    long docCount = entry.getDocCount();         // Doc count
//
//		    logger.info("key [{}], date [{}], doc_count [{}]", keyAsString, key.getYear(), docCount);
//		}
	}
	
	@Test
	public void testGeoAggr() {
		AggregationBuilder aggregation =
		        AggregationBuilders
		                .geoDistance("agg")
		                .field("address.location")
		                .point(new GeoPoint(48.84237171118314,2.33320027692004))
		                .unit(DistanceUnit.KILOMETERS)
		                .addUnboundedTo(3.0)
		                .addRange(3.0, 10.0)
		                .addRange(10.0, 500.0);
		
//		Range agg = sr.getAggregations().get("agg");
//		for (Range.Bucket entry : agg.getBuckets()) {
//		    String key = entry.getKeyAsString();    // key as String
//		    Number from = (Number) entry.getFrom(); // bucket from value
//		    Number to = (Number) entry.getTo();     // bucket to value
//		    long docCount = entry.getDocCount();    // Doc count
//
//		    logger.info("key [{}], from [{}], to [{}], doc_count [{}]", key, from, to, docCount);
//		}
	}
	
	@Test
	public void testGridAggr() {
		AggregationBuilder aggregation = AggregationBuilders
		                .geohashGrid("agg")
		                .field("address.location")
		                .precision(4);
		
//		GeoHashGrid agg = sr.getAggregations().get("agg");
//		for (GeoHashGrid.Bucket entry : agg.getBuckets()) {
//		    String keyAsString = entry.getKeyAsString(); // key as String
//		    GeoPoint key = (GeoPoint) entry.getKey();    // key as geo point
//		    long docCount = entry.getDocCount();         // Doc count
//		    logger.info("key [{}], point {}, doc_count [{}]", keyAsString, key, docCount);
//		}
	}
	
	
	
	@Before
	public void before() {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring/applicationContxt-escluster.xml");
		client = context.getBean(TransportClient.class);
	}
}
