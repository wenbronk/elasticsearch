package com.iwhere.javaes;

import org.elasticsearch.action.count.CountResponse;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.MultiSearchResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogramInterval;
import org.elasticsearch.search.sort.SortOrder;
import org.elasticsearch.search.sort.SortParseElement;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 测试searchAPI
 * @author 231
 */
public class JavaESSearchAPI {

	private TransportClient client;
	
	/**
	 * prepareSearch的方法
	 * 过滤, 分页, 条件搜索等
	 */
	@Test
	public void testPrepareSearch() {
		SearchRequestBuilder requestBuilder = client.prepareSearch("twitter", "index")
			.setTypes("tweet", "fulltext")
			.setQuery(QueryBuilders.termQuery("gender", "male"))		// query
			.setPostFilter(QueryBuilders.rangeQuery("age").from(12).to(18))	//filter
			.setFrom(0).setSize(60).setExplain(true);
		SearchResponse response = requestBuilder.execute().actionGet();
		
		// MatchAll
//		SearchResponse response = client.prepareSearch().execute().actionGet();
		explainResponse(response);
	}
	
	/**
	 * scroll 
	 * 类似光标
	 */
	@Test
	public void testScroll() {
		TermQueryBuilder termQuery = QueryBuilders.termQuery("user", "kimchy");
		
		SearchRequestBuilder requestBuilder = client.prepareSearch("twitter", "index")
								.addSort(SortParseElement.DOC_FIELD_NAME, SortOrder.DESC)
								.setScroll(new TimeValue(60000))		//设置时间
								.setSize(100);
		SearchResponse response = requestBuilder.execute().actionGet();
		explainResponse(response);
		// 实现游标的效果
		SearchResponse response2 = client.prepareSearchScroll(response.getScrollId())
			.setScroll(new TimeValue(60000)).execute().actionGet();
		if (response2.getHits().getHits().length == 0) {
			System.out.println("i am break");
		}
		
	}
	
	/**
	 * 测试multiSearch  _msearch
	 */
	@Test
	public void testMultiSearch() {
		// 创建查询1
		QueryStringQueryBuilder queryBuild1 = QueryBuilders.queryStringQuery("美国");
		SearchRequestBuilder requestBuilder1 = client.prepareSearch().setQuery(queryBuild1).setSize(1);
		// 创建查询2
		MatchQueryBuilder queryBuilder2 = QueryBuilders.matchQuery("user", "kimchy");
		SearchRequestBuilder requestBuilder2 = client.prepareSearch().setQuery(queryBuilder2);
		
		MultiSearchRequestBuilder prepareMultiSearch = client.prepareMultiSearch();
		prepareMultiSearch.add(requestBuilder1);
		prepareMultiSearch.add(requestBuilder2);
		MultiSearchResponse multiSearchResponse = prepareMultiSearch.execute().actionGet();

		long nbHits = 0;
		for (MultiSearchResponse.Item item : multiSearchResponse.getResponses()) {
			SearchResponse response = item.getResponse();
			nbHits = response.getHits().getTotalHits();
			SearchHit[] hits = response.getHits().getHits();
			System.out.println(nbHits);
		}
	}
	
	/**
	 * 聚合		 // 单独说
	 * interval 支持多种关键字, year, quarter, month, week等
	 * 
	 */
	@Test
	public void testAggregations() {
		AggregationBuilder termsBuilder = AggregationBuilders.terms("agg1").field("field");
		
		SearchRequestBuilder searchRequestBuilder = client.prepareSearch()
				.setQuery(QueryBuilders.matchAllQuery())
				.addAggregation(termsBuilder)
				// TODO dateHistogram("")
				.addAggregation(AggregationBuilders.dateHistogram("agg2")
							.field("birth")
							.interval(DateHistogramInterval.YEAR)
//							.offset("+6h") 	// 改变时区
						);
		SearchResponse response = searchRequestBuilder.execute().actionGet();
		explainResponse(response);
	}
	
	/**
	 * 手动关闭
	 */
	@Test
	public void testTerminateAfter() {
		SearchResponse response = client.prepareSearch("twitter")
						.setTerminateAfter(10000)	// 收集1w份文档后关闭
						.get();
		if (response.isTerminatedEarly()) {
			// 可查询是否提前关闭
		}
	}
	
	/**
	 * 过滤查询: 大于gt, 小于lt, 小于等于lte, 大于等于gte
	 */
	@Test
	public void testFilter() {
		SearchResponse response = client.prepareSearch("twitter").setTypes("").setQuery(QueryBuilders.matchAllQuery()) // 查询所有
				.setSearchType(SearchType.QUERY_THEN_FETCH)
//				 .setPostFilter(FilterBuilders.rangeFilter("age").from(0).to(19)
				// .includeLower(true).includeUpper(true))
				// .setPostFilter(FilterBuilderFactory
				// .rangeFilter("age").gte(18).lte(22))
				.setExplain(true) // explain为true表示根据数据相关度排序，和关键字匹配最高的排在前面
				.get();
	}

	/**
	 * 分组查询
	 */
	@Test
	public void testGroupBy() {
		client.prepareSearch("twitter").setTypes("tweet").setQuery(QueryBuilders.matchAllQuery())
				.setSearchType(SearchType.QUERY_THEN_FETCH)
				.addAggregation(AggregationBuilders.terms("user").field("user").size(0) // 根据user进行分组
				).get();
	}
	
	@Test
	public void testPrepareCount() {
		CountResponse response = client.prepareCount("twitter")
				.setQuery(QueryBuilders.termQuery("_type", "type1"))
				.execute().actionGet();
	}
	
	
	
	
	/**
	 * 对response进行解析
	 * @param response
	 */
	public void explainResponse(SearchResponse response) {
		SearchHits hits = response.getHits(); 
		for (SearchHit searchHit : hits.getHits()) {
			System.err.println("id: " + searchHit.getId());
			System.err.println("type: " + searchHit.getType());
			System.err.println("fields: " + searchHit.getFields());
			String sourceAsString = searchHit.getSourceAsString();
			System.err.println("source : " + sourceAsString);
		}
	}
	
	@Before
	public void before() {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring/applicationContxt-escluster.xml");
		client = context.getBean(TransportClient.class);
	}
}
