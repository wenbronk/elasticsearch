package com.iwhere.geosearch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.elasticsearch.action.admin.indices.create.CreateIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.yaml.snakeyaml.tokens.StreamStartToken;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * 从json文件中导入数据到es中
 * @author 231
 *
 */
public final class ImportData {

	private TransportClient client;
	
	@Test
	public void test1() throws Exception {
		createMapping("test", "catchModel");
		importData("test", "catchModel");
		System.out.println("success");
	}
	
	/**
	 * 插入https://www.elastic.co/blog/geo-location-and-search的测试数据
	 * @throws Exception 
	 */
	@Test
	public void test2() throws Exception {
		String index = "geo";
		String type = "test";
		BulkRequestBuilder prepareBulk = client.prepareBulk();
		for (int i = 0; i < 50; i++) {
			XContentBuilder source = getJson(40 + i, -71.34 + i, "my favorite family restaurant");
			prepareBulk.add(client.prepareIndex(index, type).setSource(source));
		}
		BulkResponse response = prepareBulk.execute().actionGet();
	}
	
	public XContentBuilder getJson(double lat, double lon, String text) throws IOException {
		return XContentFactory.jsonBuilder()
				.startObject()
					.startObject("pin")
						.startObject("location").field("lat", lat).field("lon", lon).endObject()
						.field("tag", "food", "family")
						.field("text", text)
					.endObject()
				.endObject();
	}
	
	
	/**
	 * 导入数据
	 * @throws Exception 
	 */
	public void importData(String index, String type) throws Exception {
		BufferedReader br = new BufferedReader(new FileReader(new File("D://catchModel.json")));
		StringBuilder sb = new StringBuilder();
		String line = null;
		while((line = br.readLine()) != null) {
			sb.append(line);
		}
		
		BulkRequestBuilder prepareBulk = client.prepareBulk();
		JSONArray parseArray = JSON.parseArray(sb.toString());
		for (Object object : parseArray) {
			// 强转为map, 否则报错  the number of object passed must be even
			Map<String, Object> source = (Map<String, Object>) object;
//			IndexResponse response = client.prepareIndex(index, type).setSource(source).get();
			XContentBuilder xContentBuilder = XContentFactory.jsonBuilder()
				.startObject()
					.field("taskName", source.get("taskName"))
					.field("sessionId", source.get("sessionId"))
					.field("geoNum", source.get("geoNum"))
					.field("geoLevel", source.get("geoLevel"))
					.field("createTime", source.get("createTime"))
					.field("endTime", source.get("endTime"))
					.startObject("location").field("lat", source.get("lbLat"))
							.field("lon", source.get("lbLng"))
//					.field("location", source.get("lbLat") + "," + source.get("lbLng"))
//							XContentFactory.jsonBuilder()
//										.startObject()
//											.field("lat", source.get("lbLat"))
//											.field("lon", source.get("lblng"))
//										.endObject())
					.endObject();
			prepareBulk.add(client.prepareIndex(index, type).setSource(xContentBuilder));
		}
		BulkResponse response = prepareBulk.get();
		System.out.println(response);
	}
	
	
	/**
	 * 创建mapping, 添加ik分词器等, 相当于创建数据库表
	 * 索引库名: indices
	 * 类型 	 : mappingType
	 * field("indexAnalyzer", "ik"): 字段分词ik索引
	 * field("searchAnalyzer", "ik"): ik分词查询
	 * 
	 * geohash_prefix	// geohash 使用的
	 * 
	 * @throws Exception 
	 */
	public void createMapping(String indices, String type) throws Exception {
		
		// 创建index
		Map<String, Object> settings = new HashMap<>();
		settings.put("number_of_shards", 4);	// 分片数量
		settings.put("number_of_replicas", 0);	// 复制数量, 导入时最好为0, 之后2-3即可
		settings.put("refresh_interval", "10s");// 刷新时间
		
		CreateIndexRequestBuilder prepareCreate = client.admin().indices().prepareCreate(indices);
		prepareCreate.setSettings(settings);
		
		// 创建mapping
		XContentBuilder mapping = XContentFactory.jsonBuilder()
			.startObject()
				.startObject(type)
//					.startObject("_ttl")//有了这个设置,就等于在这个给索引的记录增加了失效时间,  
//	                //ttl的使用地方如在分布式下,web系统用户登录状态的维护.  
//				        .field("enabled", true)//默认的false的  
//				        .field("default", "5m")//默认的失效时间,d/h/m/s 即天/小时/分钟/秒  
//				        .field("store", "yes")  
//				        .field("index", "not_analyzed")  
//			        .endObject() 
//				 	.startObject("_timestamp")//这个字段为时间戳字段.即你添加一条索引记录后,自动给该记录增加个时间字段(记录的创建时间),搜索中可以直接搜索该字段.  
//		                .field("enabled", true)  
//		                .field("store", "no")  
//		                .field("index", "not_analyzed")  
//	                .endObject() 
				.startObject("properties")
				.startObject("taskName").field("type", "keyword").field("analyzer", "ik").field("searchAnalyzer", "ik").endObject()
				.startObject("sessionId").field("type", "text").endObject()
				.startObject("geoNum").field("type", "text").endObject()
				.startObject("grandPaGeoNum").field("type", "text").endObject()
				.startArray("sonGeoNum").endArray()
				.startObject("geoLevel").field("type", "long").endObject()
				.startObject("state").field("type", "integer").endObject()
				.startObject("createTime").field("type", "date").endObject()
				.startObject("endTime").field("type", "date").endObject()
				.startObject("location")
					.field("type", "geo_point").field("geohash_prefix", true).field("geohash_precision", "1km").endObject()/*.field("lat_lon", true)*/.endObject()
			.endObject().endObject();
		System.out.println(mapping.string());
//		PutMappingResponse actionGet = client.admin().|indices().preparePutMapping(indices).setType(indices).setSource(mapping).execute().actionGet();
		prepareCreate.addMapping(type, mapping);
		CreateIndexResponse response = prepareCreate.execute().actionGet();
		System.out.println(response);
	}
	
	@Before
	public void before() {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring/applicationContxt-escluster.xml");
		client = context.getBean(TransportClient.class);
	} 
}
