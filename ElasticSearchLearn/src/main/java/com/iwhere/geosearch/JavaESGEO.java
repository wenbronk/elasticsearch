package com.iwhere.geosearch;

import java.net.InetSocketAddress;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.geo.GeoDistance;
import org.elasticsearch.common.geo.GeoPoint;
import org.elasticsearch.common.geo.ShapeRelation;
import org.elasticsearch.common.geo.builders.ShapeBuilder;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.unit.DistanceUnit;
import org.elasticsearch.index.query.GeoBoundingBoxQueryBuilder;
import org.elasticsearch.index.query.GeoDistanceQueryBuilder;
import org.elasticsearch.index.query.GeoDistanceRangeQueryBuilder;
import org.elasticsearch.index.query.GeoPolygonQueryBuilder;
import org.elasticsearch.index.query.GeoShapeQueryBuilder;
import org.elasticsearch.index.query.GeohashCellQuery.Builder;
import org.elasticsearch.index.query.QueryBuilders;
import org.junit.Before;
import org.junit.Test;

/**
 * 使用dsl查询
 * @author 231
 */
public class JavaESGEO {

	private TransportClient client;
	
	/**
	 * Caused by: [test] QueryParsingException[Field [location] is not a geo_shape]
	 */
	@Test
	public void testGeoShapeQuery() {
		GeoShapeQueryBuilder geoShapeQuery = QueryBuilders.geoShapeQuery("location", ShapeBuilder.newMultiPoint()
					.point(0, 0)
					.point(0, 10)
					.point(10, 10)
					.point(10, 0)
					.point(0, 0)
					, ShapeRelation.WITHIN);
		System.out.println(geoShapeQuery);
		
		SearchResponse response = client.prepareSearch("geo")
						.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
						.setQuery(geoShapeQuery).get();
	}
	
	/**
	 * Caused by: java.lang.IllegalStateException: Shape with name [AVqw3mb-kOe4Yke4p-lh] found but missing shape field
	 */
	@Test
	public void testGeoShapeQuery1() {
		GeoShapeQueryBuilder queryBuilder = QueryBuilders.geoShapeQuery(
				"model.location", 	// field
				"AVqxrMyikOe4Yke4p_Wx", // id of document
				"catchModel", ShapeRelation.WITHIN)	// type, relation
			.indexedShapeIndex("test")	// name of index
			.indexedShapePath("location");	// filed specified as path
		SearchResponse response = client.prepareSearch()
				.setQuery(queryBuilder).execute().actionGet();
		String string = response.getHits().getHits().toString();
		System.out.println(string);
	}
	
	
	/**
	 * 使用 BoundingBoxQuery进行查询
	 */
	@Test
	public void testGeoBoundingBoxQuery( ){
		GeoBoundingBoxQueryBuilder queryBuilder = QueryBuilders.geoBoundingBoxQuery("location")
			.topRight(40.0, 117)		
			.bottomLeft(39.9, 116);
		SearchResponse searchResponse = client.prepareSearch("test")
				.setQuery(queryBuilder).get();
		System.out.println(searchResponse);
		System.err.println(searchResponse.getHits().totalHits());
	}
	
	/**
	 * distance query  查询
	 */
	@Test
	public void testDistanceQuery() {
		GeoDistanceQueryBuilder queryBuilder = QueryBuilders.geoDistanceQuery("location")
			.point(40, 116.5)
			.distance(20, DistanceUnit.KILOMETERS)
			.optimizeBbox("memory")
			.geoDistance(GeoDistance.ARC);
		SearchResponse response = client.prepareSearch("geo", "test")
				.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
				.setQuery(queryBuilder).execute().actionGet();
		System.out.println(response);
		System.err.println(response.getHits().totalHits());
	}
	
	/**
	 * 环形查询
	 */
	@Test
	public void testDistanceRangeQuery() {
		GeoDistanceRangeQueryBuilder queryBuilder = QueryBuilders.geoDistanceRangeQuery("location")
							.point(40,  116.5)			// 中心点
							.from("20km")				// 内环
							.to("25km")					//外环
							.includeLower(true)			// 包含上届
							.includeUpper(true)			// 包含下届
							.optimizeBbox("memory")		// 边界框
							.geoDistance(GeoDistance.SLOPPY_ARC);
		SearchResponse response = client.prepareSearch("test")
			.setSearchType(SearchType.DFS_QUERY_AND_FETCH)
			.setQuery(queryBuilder).execute().actionGet();
		System.out.println(response);
		System.out.println(response.getHits().totalHits());
	}
	
	/**
	 * 多边形查询
	 */
	@Test
	public void testPolygonQuery() {
		GeoPolygonQueryBuilder queryBuilder = QueryBuilders.geoPolygonQuery("location")
								.addPoint(39, 116)
								.addPoint(39, 117)
								.addPoint(40, 117);
		SearchResponse response = client.prepareSearch("test", "geo")
			.setQuery(queryBuilder).get();
		System.out.println(response);
		System.err.println(response.getHits().totalHits());
	}
	
	/**
	 * geoHash查询
	 * 要使用, 需要先开启
	 *  "location": {
          "type":               "geo_point",
          "geohash_prefix":     true, 
          "geohash_precision":  "1km" // 精度, 可在mapping中指定, 也可在代码中指定
	 */
	@Test
	public void testGeoHashCellQuery() {
		Builder precision = QueryBuilders.geoHashCellQuery("location", 
				new GeoPoint(39.9, 116))
			.neighbors(true)
			.precision(3);
		SearchResponse response = client.prepareSearch("test")
					.setQuery(precision).get();
		System.out.println(response);
		System.err.println(response.getHits().totalHits());
	}
	
	
	
	
	
	@Before
	public void testBefore() {
		Settings settings = Settings.settingsBuilder().put("cluster.name", "wenbronk_escluster")
					.put("client.transport.sniff", true).build();
		client = TransportClient.builder().settings(settings).build()
				 .addTransportAddress(new InetSocketTransportAddress(new InetSocketAddress("192.168.50.37", 9300)));
		System.out.println("success to connect escluster");
	}
}
